use crate::toaster::{State, Toaster};

#[derive(Debug)]
pub struct SuperToaster {
    base_toaster: Toaster,
    temperature: u64,
}

impl SuperToaster {
    pub fn new() -> Self {
        SuperToaster {
            base_toaster: Toaster::new(Toaster {
                farbe: (230, 212, 29),
                schaechte: 10,
                toast_zeit: 19,
                toast_state: State::Ungetoastet,
                toast_anzahl: 0,
            }),
            temperature: 30,
        }
    }
    pub fn super_toast(&mut self) {
        self.base_toaster.toast_reintun(8);
        self.temperatur_erhoehen();

        self.base_toaster.toasten();
    }

    pub fn temperatur_erhoehen(&mut self) {
        if self.temperature >= 500 {
            panic!("Dein Toaster ist heisser als die Sonne");
        }
    }
}
