// This will be the file for the toaster

// anzahl.toasts, toast.zustand, schaechte, farbe, zeit
// toast_reintun(self, anzahl), toast.auswerfen(anzahl), toasten, toast.zeiteinstellen
// 0 ungetoastet, <=15 leicht getoastet, > 15 stark getoastet, > 30 verbrannt
use std::{io::Write, thread, time};

#[derive(Debug)]
pub enum State {
    Ungetoastet,
    LeichtGetoastet,
    StarkGetoastet,
    Verbrannt,
}
#[derive(Debug)]
pub struct Toaster {
    pub farbe: (u8, u8, u8),
    pub schaechte: u8,
    pub toast_zeit: u64,
    pub toast_state: State,
    pub toast_anzahl: u8,
}

impl Toaster {
    pub fn new(toaster: Toaster) -> Toaster {
        return toaster;
    }
    pub fn toast_reintun(&mut self, toast_anzahl: u8) {
        if toast_anzahl <= self.schaechte {
            self.toast_state = State::Ungetoastet;
            self.toast_anzahl = toast_anzahl;
        }
    }
    fn toast_zustand(&mut self) {
        if self.toast_zeit == 0 {
            self.toast_state = State::Ungetoastet
        } else if self.toast_zeit > 0 && self.toast_zeit <= 15 {
            self.toast_state = State::LeichtGetoastet
        } else if self.toast_zeit > 15 && self.toast_zeit <= 30 {
            self.toast_state = State::StarkGetoastet
        } else if self.toast_zeit > 30 {
            self.toast_state = State::Verbrannt
        } else {
            println!("Das hätte nicht passieren sollen!");
        }
        println!("\nDer Toastzustand ist: {:?}", self.toast_state);
    }

    pub fn toast_auswerfen(&mut self) {
        self.toast_anzahl = 0;
        println!("Alle Toasts wurden ausgeworfen");
    }

    pub fn toasten(&mut self) {
        self.toast_reintun(self.toast_anzahl);
        let one_sec = time::Duration::from_secs(1);
        let mut count = 0;

        if self.toast_anzahl > 0 && self.toast_anzahl <= self.schaechte {
            loop {
                count = count + 1;
                thread::sleep(one_sec);
                print!("\rVergangene Zeit: {count} Sekunden");
                std::io::stdout().flush().unwrap();
                if count >= self.toast_zeit {
                    self.toast_zustand();
                    self.toast_auswerfen();
                    break;
                }
            }
        } else {
            println!("Irgendwas ist schief gelaufen");
        }
    }
}
