mod super_toaster;
mod toaster;

use super_toaster::SuperToaster;
use toaster::{State, Toaster};

fn main() {
    let mut toaster_gruen = Toaster::new(Toaster {
        farbe: (0, 255, 0),
        schaechte: 2,
        toast_zeit: 5,
        toast_state: State::Ungetoastet,
        toast_anzahl: 0,
    });
    let mut toaster_rot = Toaster::new(Toaster {
        farbe: (255, 0, 0),
        schaechte: 3,
        toast_zeit: 10,
        toast_state: State::Ungetoastet,
        toast_anzahl: 0,
    });
    let mut toaster_orange = Toaster::new(Toaster {
        farbe: (255, 128, 0),
        schaechte: 6,
        toast_zeit: 31,
        toast_state: State::Ungetoastet,
        toast_anzahl: 0,
    });
    let mut super_toaster = SuperToaster::new();
    toaster_gruen.toast_reintun(1);
    // toaster_gruen.toasten();
    toaster_rot.toast_reintun(2);
    toaster_orange.toast_reintun(4);
    println!("{:?}", super_toaster);
    super_toaster.super_toast();
    println!("\n\ngruener toaster, values: {:?}", toaster_gruen);
    println!("roter toaster, values: {:?}", toaster_rot);
    println!("orangener toaster, values: {:?}", toaster_orange);
}
